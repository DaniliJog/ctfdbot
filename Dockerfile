FROM python:3.12.0a2-slim-bullseye

COPY ./requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

WORKDIR /app
COPY ./main.py /app/main.py

CMD python3 /app/main.py

