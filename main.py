# std lib
import threading
import traceback
import time
import random
from os import getenv

import urllib.parse

# deps
import yaml
import httpx
from rich import print

# coordinate restarting between threads
killevent = threading.Event()

with open(getenv("CTFDBOT_CONFIG_PATH", "config.yaml")) as f:
    config = yaml.safe_load(f)

assert "admins" in config
admins = config["admins"]

assert "matrix" in config
assert "username" in config["matrix"]
username = config["matrix"]["username"]
assert "password" in config["matrix"]
password = config["matrix"]["password"]
assert "homeserver" in config["matrix"]
homeserver = config["matrix"]["homeserver"]
assert "chatroomalias" in config["matrix"]
chatroomalias = config["matrix"]["chatroomalias"]
assert "ctfd" in config
assert "ctfd_name" in config["ctfd"]
ctfd_name = config["ctfd"]["ctfd_name"]
assert "ctfd_url" in config["ctfd"]
ctfd_url = config["ctfd"]["ctfd_url"]
assert "ctfd_apikey" in config["ctfd"]
ctfd_apikey = config["ctfd"]["ctfd_apikey"]

event_type = "email.internetpost.ctfdbot"


####################
# MATRIX FUNCTIONS #
####################

def matrix_request(method, url, **kwargs):
    try:
        r = httpx.request(method, url, **kwargs, timeout=None)
    except httpx.HTTPError as e:
        # some kind of network error happened
        print(f"HTTPError: {repr(e)}")
        retry_delay = 1
        print(f"Sleeping {retry_delay} seconds and retying.")
        time.sleep(retry_delay)
        # recurse after delay
        return matrix_request(method, url, **kwargs)

    # Wait as long as server wants us to
    if r.status_code == httpx.codes.TOO_MANY_REQUESTS:
        print(f"Too many requests. retry after: {r.json()['retry_after_ms']}ms")
        print(r.json())
        time.sleep(r.json()["retry_after_ms"] / 1000)
        return matrix_request(method, url, **kwargs)

    if not r.status_code == httpx.codes.OK:
        print(f"request failed: {method=}, {url=}, {kwargs=}")
        print(r.json())
        r.raise_for_status()

    return r


def login(homeserver, username, password):
    """Log in to Matrix homeserver"""
    # get access token
    r = matrix_request(
        "post",
        f"{homeserver}/_matrix/client/v3/login",
        json={
            "type": "m.login.password",
            "identifier": {"type": "m.id.user", "user": username},
            "password": password,
        },
    )
    if not r.status_code == httpx.codes.OK:
        print(f"{r.status_code} on login")
        r.raise_for_status()
    return r.json()["access_token"]


def join_room(homeserver, access_token, chatroomalias):
    """Join Matrix room"""
    r = matrix_request(
        "post",
        f"{homeserver}/_matrix/client/v3/join/{urllib.parse.quote(chatroomalias, safe='')}",
        headers={"Authorization": f"Bearer {access_token}"},
    )
    if not r.status_code == httpx.codes.OK:
        print(f"{r.status_code} on join: {r.json()}")
        r.raise_for_status()
    return r.json()["room_id"]


# TODO
# Implement parent and type: room
def create_room(homeserver, access_token, aliasname, admins):
    """Create Matrix room"""
    event_content = {
        "visibility": "private",
        "room_alias_name": aliasname,
        "name": aliasname,
        "invite": admins,
        "creation_content": {"m.federate": True, "type": "m.space"},
        "initial_state": [
            # Make the room invite only.
            {
                "type": "m.room.join_rules",
                "content": {"join_rule": "invite"},
            },
            # Make future room history visible to members since
            # they were invited.
            {
                "type": "m.room.history_visibility",
                "content": {"history_visibility": "invited"},
            },
        ],
        # Make sender admin in new room
        "power_level_content_override": {
            "users": {
                admins[0]: 100,
                "@ctfdbot:pyjam.as": 100,
            }
        },
    }
    r = matrix_request(
        "post",
        f"{homeserver}/_matrix/client/v3/createRoom",
        headers={"Authorization": f"Bearer {access_token}"},
        json=event_content,
    )
    rjson = r.json()

    if not r.json()["room_id"]:
        errcode = rjson.get("errcode", "")
        if errcode == "M_ROOM_IN_USE":
            print(f"Sorry, {aliasname} is already used by someone else.")
        else:
            error_msg = rjson.get("error", "no error message")
            print(f"Unknown error. Error from homeserver: {error_msg}.")
    return r.json()["room_id"]


def send_message(homeserver, access_token, room_id, message, formatted_body=None):
    txn_id = random.randint(0, 1_000_000_000)
    event_content = {"msgtype": "m.text", "body": message} | (
        {"format": "org.matrix.custom.html", "formatted_body": formatted_body}
        if formatted_body
        else {}
    )
    r = matrix_request(
        "put",
        f"{homeserver}/_matrix/client/v3/rooms/{room_id}/send/m.room.message/{txn_id}",
        headers={"Authorization": f"Bearer {access_token}"},
        json=event_content,
    )
    if not r.status_code == httpx.codes.OK:
        print(event_content)
        print(f"{r.status_code} while sending message: {r.json()}")
        r.raise_for_status()
    return r.json()["event_id"]


def react_to_message(homeserver, access_token, room_id, event_id, message):
    txn_id = random.randint(0, 1_000_000_000)
    event_content = {
        "m.relates_to": {
            "event_id": f"{event_id}",
            "key": f"{message}",
            "rel_type": "m.annotation",
        },
    }

    print(event_content)
    r = matrix_request(
        "put",
        f"{homeserver}/_matrix/client/v3/rooms/{room_id}/send/m.reaction/{txn_id}",
        headers={"Authorization": f"Bearer {access_token}"},
        json=event_content,
    )
    if not r.status_code == httpx.codes.OK:
        print(f"{r.status_code} while sending message: {r.json()}")
        r.raise_for_status()


def sync(homeserver, access_token, since=None):
    r = matrix_request(
        "get",
        f"{homeserver}/_matrix/client/v3/sync",
        params={"timeout": 5000} | ({"since": since} if since else {}),
        headers={"Authorization": f"Bearer {access_token}"},
    )
    if not r.status_code == httpx.codes.OK:
        print(f"{r.status_code} from sync.")
        print(f"Sync Response Content:\n{r.text}")
        r.raise_for_status()
    return r.json()


def sync_generator(homeserver, access_token):
    """Call Matrix /sync forever, react to events"""
    print("Starting sync worker...")
    since = None
    while True:

        if since is None:
            # get initial since token (and first events)
            # dont process events - we don't care about the past
            r = sync(homeserver, access_token)
            since = r["next_batch"]
            continue

        # call sync (runs after first loop)
        r = sync(homeserver, access_token, since=since)
        since = r["next_batch"]

        if "rooms" not in r:
            # print("No events in sync result")
            continue

        # join all invites
        for invite_room_id in r["rooms"].get("invite", []):
            print(f"Joining room {invite_room_id}")
            join_room(homeserver, access_token, invite_room_id)

        if "join" not in r["rooms"]:
            print("No events from joined rooms")
            continue 

        # react to events in joined rooms
        for room_id, room in r["rooms"]["join"].items():
            for event in room["timeline"]["events"]:
                if event["type"] != "m.room.message":
                    print("Skipping non-message event")
                    continue

                if event["content"]["msgtype"] != "m.text":
                    # print("Skipping non-text message")
                    continue

                yield room_id, event


def chat_worker(homeserver, access_token):
    for room_id, event in sync_generator(homeserver, access_token):
        plaintext = event["content"]["body"]
        match plaintext.split():
            case ["!help", *_]:
                help(homeserver, access_token, room_id)
            case ["!ping", *_]:
                ping(homeserver, access_token, room_id, event)
        if killevent.is_set():
            return


def ping(homeserver, access_token, room_id, event):
    react_to_message(homeserver, access_token, room_id, event["event_id"], "pong")

def help(homeserver, access_token, room_id):
    send_message(
        homeserver,
        access_token,
        room_id,
        "Hello 👋\n\nYou can run the following commands:\n\n- `!help` shows this message\n- `!ping` to check if I am running.",
        "<p>Hello 👋</p>\n<p>You can run the following commands:</p>\n<ul>\n<li><code>!help</code> shows this message</li>\n<li><code>!ping</code> to check if I am running.</li>\n</ul>\n",
    )


##################
# CTFD FUNCTIONS #
##################

def get_challenges(ctfd_url, ctfd_apikey):
    url = f"{ctfd_url}/api/v1/challenges"
    headers = {'Authorization': f"Token {ctfd_apikey}"}
    r = httpx.get(url, headers=headers)
    return r.json()


if __name__ == "__main__":
    # do da matrix dance
    access_token = login(homeserver, username, password)
    room_id = join_room(homeserver, access_token, chatroomalias)

    # do da ctfd dance
    ctfd_space_id = create_room(homeserver, access_token, ctfd_name, admins)
    ## TODO
    # challenges = get_challenges(ctfd_url, ctfd_apikey)
    ## Create spaces from categories
    # for categori in challenges[categories]: #Unique
    #     categori_space_id = create_room(homeserver, access_token, categori.name, admins, parent=ctfd_space_id)
    #     for challenge in challenges[categori]{challenges]:
    #         challenge_room_id = create_room(homeserver, access_token, challenge.name, admins, parent=categori_space_id, room=true)

    def exception_wrapper(f):
        """Post uncaught exceptions from worker in matrix and die"""

        def wrapped(*args, **kwargs):
            try:
                f(*args, **kwargs)
            except Exception as e:
                # create exception message
                msg = f"🤖 Sad robot.\nRobot error: {repr(e)}"
                print(msg)
                e_str = traceback.format_exc()
                format_msg = (
                    f"🤖 Sad robot.<br>Robot error:<pre><code>{e_str}</code></pre>"
                )

                # send exception message in room
                sad_robot_event_id = send_message(
                    homeserver, access_token, room_id, msg, format_msg
                )
                react_to_message(
                    homeserver, access_token, room_id, sad_robot_event_id, "😢"
                )

                # die
                killevent.set()
                raise e

        return wrapped

    # worker that responds to chatbot commands
    chatter = threading.Thread(
        target=exception_wrapper(chat_worker), args=(homeserver, access_token)
    )
    chatter.start()

    # worker checking ctfd TODO
    # hacker = threading.Thread(
    #     targert=some_function,
    #     args=(someargs, otherargs, proberly_a_token, and_maybe_a_url),
    # )
    # hacker.start()

    # wait for death signal
    killevent.wait()
    print(f"💀 Got death signal")
    quit(1)  # 💀 die

